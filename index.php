<!DOCTYPE html>
<html lang="fi">
<?php


//save these as files first
//parties: https://www.vaalikone.fi/kunta2021/api/parties
//districts: https://www.vaalikone.fi/kunta2021/api/districts turku: 853
//all candidates: https://www.vaalikone.fi/kunta2021/api/candidates

//individual caldidates
//candidates: https://www.vaalikone.fi/kunta2021/api/candidates/14740

///vessakysmys: 
/*
"answer": 4,
"explanation": "Sukupuolineutraalit WC-tilat eivät ole pois keneltäkään. 
Muutos ei kaikkialla kuitenkaan tapahdu vain ovikylttiä vaihtamalla,
vaan se kannattaisi toteuttaa remontoinnin ja uudisrakentamisen yhteydessä.",
"questionId": 217

1 = täysin eri mieltä
5 = täysin samaa mieltä
*/
$districts =  json_decode( file_get_contents("districts.json"));
$parties =  json_decode( file_get_contents("parties.json"));
$fh = json_decode( file_get_contents("candidates.json"));


foreach($districts as $dd) {
	$district_arr[$dd->id] = $dd->name;
}


$ratings = array(
"1" => "täysin eri mieltä",
"2" => "jokseenkin eri mieltä",
"3" => "eos", 
"4" => "jokseenkin samaa mieltä", 
"5" => "täysin samaa mieltä"
);

if(!isset($_GET['districtId'])) {
	foreach($districts as $d) {
		print "<a href='?districtId=".$d->id."'>".$d->name."</a><br>";
	}
}

if(isset($_GET['districtId'])) {
	print "<h1>".$district_arr[$_GET['districtId']]."</h1>";
	print "<p>Koulujen ja kirjastojen wc-tiloja on muutettu Suomessakin sukupuolineutraaleiksi trans- ja muunsukupuolisten tasavertaisen kohtelun takia. ".
        "Kotikuntani tulee muuttaa koulujen ja kirjastojen wc-tilat sukupuolineutraaleiksi tulevan valtuustokauden loppuun mennessä.</p>";
	foreach($fh as $candidate) {
		if($candidate->districtId == $_GET['districtId']) {

			$c =  json_decode( file_get_contents("https://www.vaalikone.fi/kunta2021/api/candidates/".$candidate->id));
				
			foreach($c->candidateAnswers as $a) {
				if($a->questionId == '217' && ($a->answer == "1" ||  $a->answer == "2")) {
					print "<a target='_blank' href='https://www.vaalikone.fi/kunta2021/is/ehdokkaat/".$candidate->districtId."/".$c->id."'>".$c->firstName." ".$c->lastName." (".$c->party->name.")</a> ";
					print "<p><b>".$ratings[$a->answer]."</b> ";
					print $a->explanation."<p>";

				}
			}

		}
	}
}



?>